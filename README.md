## Synopsis

IotApp is a IoT device manager UI that runs on a PiBox-based Ironman system.

IotApp watches a directory of registration files for updates and displays a set of device states.  It accepts input via a touchscreen to change device states by issuing Ironman protocol messages.

IotApp is based on GTK and Cairo.

## Build

IotApp can be built and tested on a local Linux box using autoconf.  A cross compiled version can be built using the cross.sh wrapper script.

### Local Build and Test

To build locally for testing, use the following commands.

    autoreconf -i
    ./configure
    make
    src/iotapp -T -v3

The last command will run iotapp in test mode and use verbosity level three for debugging.  

For more information on command line options use the following command.

    src/iotapp -?

### Cross compile and packaging

To cross compile the application use the cross.sh script.  To get a simple usage statement for this script use the following command.

    ./cross.sh -?

Use of the cross compile script requires specifying three components.

1. The path to the cross compiler toolchain directory.
1. The path to the PiBox root file system staging directory.
1. The path to the opkg tools on the host system.

The first two components can be found under the respective build trees of the PiBox Development Platform build.  They may also be distributed as part of the packaged PiBox Development Platform.  Either way, the argument for these options is a directory path.

The last component requires installation of the opkg tools on the host.  These can be built from the PiBox Development Platform using the following target.

    make opkg
    make opkg-install

Additional information for building can be found in the INSTALL file.

## Installation

IotApp is packaged in an opkg format.  After cross compiling look in the opkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boo the system.  The use the following command.

    opkg install <path to file>/iotapp_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/iotapp_1.0_arm.opk

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD

