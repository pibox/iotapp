/*******************************************************************************
 * iotapp
 *
 * db.c:  Functions for dealing with devices
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_C

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <pibox/pibox.h>
#include <pibox/parson.h>
#include <pibox/pmsg.h>
#include <pibox/log.h>

#include "iotapp.h"

GSList  *deviceList = NULL;

// Parson components that we use to store registration information
static JSON_Value  *root_value = NULL;     // Data block wrapping JSON objects
static JSON_Object *root_object = NULL;    // root JSON object from root_value

/*
 *========================================================================
 * Name:   getDevices
 * Prototype:  void getDevices( void )
 *
 * Description:
 * Requests list of devices from piboxd and stores them locally.
 *========================================================================
 */
void
getDevices ( void )
{
    int     rc;
    char    *result=NULL;
    gint    resultSize=0;
    char    *ptr=NULL;

    // Retrieve list of devices
    rc = piboxMsg(MT_DEVICE, MA_GETALL, 0, NULL, 0, NULL, &resultSize, &result);                                                      
    if ( (rc != 0) || (result == NULL) )
    {
        piboxLogger(LOG_ERROR, "Failed to retrieve device list from daemon.\n");
        return;
    }

    // Clean up previous query results
    if ( root_value != NULL )
    {
        json_value_free(root_value);
        root_value = NULL;
        root_object = NULL;
    }

    // Put into local JSON structure.
    root_value = json_parse_string(result);
    root_object = json_value_get_object(root_value);
    if (root_object == NULL) 
    {
        piboxLogger(LOG_ERROR, "Root object is null.\n");
    }

    piboxLogger(LOG_INFO, "Retrieved device configs from daemon: %s\n", result);
    piboxLogger(LOG_INFO, "Identity: %s\n", json_object_dotget_string(root_object, "192-168-101-4.identity"));

    ptr = json_serialize_to_string_pretty( root_value );
    piboxLogger(LOG_INFO, "Pretty Serialized: \n%s\n", ptr);
    free(ptr);
}

/*
 *========================================================================
 * Name:   getDevice
 * Prototype:  void getDevice( char *device_ip )
 *
 * Description:
 * Request configuration for the specified device.
 *========================================================================
 */
JSON_Value *
getDevice ( char *device_ip )
{
    piboxLogger(LOG_INFO, "Looking for device: %s\n", device_ip);
    return json_object_get_value( root_object, device_ip );
}

/*
 *========================================================================
 * Name:   populateList
 * Prototype:  void populateList( GtkWidget * )
 *
 * Description:
 * Fills a list widget with device names.
 *========================================================================
 */
void
populateList (GtkListStore *listStore)
{
    GtkTreeIter     iter;
    int             cnt;
    int             idx;
    const char      *device_ip;
    const char      *identity;
    JSON_Object     *device;

    if ( root_object == NULL )
        return;

    // Clear the list, in case it has been populated recently.
    gtk_list_store_clear(listStore);

    // Acquire an iterator on the list
    gtk_list_store_append (listStore, &iter);

    // Iterate over root_object for each device IP
    cnt = json_object_get_count( root_object );
    piboxLogger(LOG_INFO, "Number of devices: %d\n", cnt);
    for(idx=0; idx<cnt; idx++)
    {
        // Pull the IP address
        device_ip = json_object_get_name( root_object, idx);                                         
        piboxLogger(LOG_INFO, "Device IP: %s\n", device_ip);

        // Get it's object
        device = json_object_get_object( root_object, device_ip);                                         

        // Get it's identity
        identity = json_object_get_string( device, "identity");                                         
        piboxLogger(LOG_INFO, "Device identity: %s\n", identity);

        // Add the device name to the end of the list.
        gtk_list_store_set(listStore, &iter, 0, device_ip, 1, identity, -1);
    }
}

