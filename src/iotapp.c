/*******************************************************************************
 * IoT App
 *
 * iotapp.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define IOTAPP_C

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pthread.h>
#include <errno.h>
#include <pibox/log.h>
#include <pibox/utils.h>
#include <pibox/parson.h>

#include "iotapp.h" 

GtkWidget   *window;
GtkWidget   *notebook;
GtkWidget   *devicePage;
GtkWidget   *featureTable;
GtkWidget   *view;
int         currentRow = 0;
int         firstTime = 1;

// The home key is the key that exits the app.
guint homeKey = -1;

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void 
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    // Read in /etc/pibox-keysysm
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        // Grab second token
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        // Set the home key
        if ( strncasecmp("home", action, 4) == 0 )
        {
            piboxLogger(LOG_INFO, "keysym = %s\n", keysym);
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean 
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{      
    int page = -1;

    switch(event->keyval) 
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                gtk_main_quit();
                return(TRUE);
            }
            break;

        /*
         * HOME key will back up one page, either to device list or
         * exit the app.
         */
        case GDK_KEY_Home:
            page = gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook));
            if ( page == 1 )
            {
                gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), 0);
                return(TRUE);
            }
            else
            {
                piboxLogger(LOG_INFO, "Home key\n");
                gtk_main_quit();
                return(TRUE);
            }
            break;

        default:
            if ( event->keyval == homeKey )
            {   
                page = gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook));
                if ( page == 1 )
                {
                    gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), 0);
                    return(TRUE);
                }
                else
                {
                    piboxLogger(LOG_INFO, "Keysym configured home key\n");
                    gtk_main_quit();
                }
                return(TRUE);
            }
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   freeFeature
 * Prototype:  void freeFeature( gpointer )
 *
 * Description:
 * Clean up FEATURE_T entries from a list.
 *========================================================================
 */
void
freeFeature ( gpointer *data )
{
    FEATURE_T *entry = (FEATURE_T *)data;
    if ( entry )
    {
        if ( entry->name ) free(entry->name);
        if ( entry->type ) free(entry->type);
        free(entry);
    }
}

/*
 *========================================================================
 * Name:   getFeatureList
 * Prototype:  GSList *getFeatureList( JSON_Object *features )
 *
 * Description:
 * Create a list of features with their types.
 *========================================================================
 */
GSList *
getFeatureList ( JSON_Object *features )
{
    FEATURE_T   *entry;
    GSList      *list = NULL;
    const char  *name;
    const char  *value;
    int         cnt;
    int         idx;

    cnt = json_object_get_count( features );
    piboxLogger(LOG_INFO, "Number of features: %d\n", features);
    for(idx=0; idx<cnt; idx++)
    {
        // Pull a name from the object
        name = json_object_get_name( features, idx);                                         
        piboxLogger(LOG_INFO, "Name: %s\n", name);

        // If the name has no "-" then it's a field name, so add an entry.
        if ( index(name, '-') == NULL )
        {
            entry = (FEATURE_T *)malloc(sizeof(FEATURE_T));
            entry->name = strdup(name);
            value = json_object_get_string( features, name);                                         
            entry->type = strdup(value);
            list = g_slist_insert(list, entry, -1);
        }
    }

    return list;
}

/*
 *========================================================================
 * Name:   genFields
 * Prototype:  void genFields( gpointer *featureList, gpointer *features )
 *
 * Description:
 * Generate the UI components based on the feature list.
 *========================================================================
 */
void
genFields ( gpointer featureList, gpointer data )
{
    JSON_Object *features = (JSON_Object *)data;
    FEATURE_T   *entry = (FEATURE_T *)featureList;
    const char  *name;
    const char  *value;
    int         cnt;
    int         idx;
    char        *prefix;
    char        *ptr;
    char        *buf;
    char        *comboPtr;
    char        *lowStr;
    char        *hiStr;
    const char  *display = NULL;
    const char  *currentSetting = NULL;
    const char  *possibleValues = NULL;
    char        *possibleValuesDup = NULL;
    GtkWidget   *label;
    GtkWidget   *widget;

    // Find the feature name
    ptr = strdup(entry->name);
    prefix = strtok(ptr, "-");

    // Find related JSON name's 
    cnt = json_object_get_count( features );
    for(idx=0; idx<cnt; idx++)
    {
        // Pull a name from the object
        name  = json_object_get_name( features, idx );
        piboxLogger(LOG_INFO, "Name: %s\n", name);

        // If the name has the specified prefix then create a field for it.
        if ( strstr(name, prefix) == name )
        {
            value = json_object_get_string( features, name );
            piboxLogger(LOG_INFO, "Value: %s\n", name);
            buf = (char *)malloc(strlen(prefix)+strlen(entry->type)+1);

            // Check for the display name
            sprintf(buf, "%s-display", prefix);
            if ( strcmp(buf, name) == 0 )
            {
                display = value;
                free(buf);
                continue;
            }

            // Check for the current value
            sprintf(buf, "%s-value", prefix);
            if ( strcmp(buf, name) == 0 )
            {
                currentSetting = value;
                free(buf);
                continue;
            }

            // Check for the possible values this feature can have
            sprintf(buf, "%s-%s", prefix, entry->type);
            if ( strcmp(buf, name) == 0 )
            {
                possibleValues = value;
                free(buf);
                continue;
            }
        }
    }
    free(ptr);

    // Create a label with the display name
    if ( display != NULL )
        label = gtk_label_new(display);
    else
        label = gtk_label_new(entry->name);
    gtk_widget_show(label);

    // Create the input field with possible types and current setting.
    if ( strcmp(entry->type, "set") == 0 )
    {
        // Combo box 
        if ( possibleValues != NULL )
        {
            widget = gtk_combo_box_text_new();
            possibleValuesDup = strdup(possibleValues);
            comboPtr = strtok(possibleValuesDup, ",");
            idx = 0;
            while ( comboPtr != NULL )
            {
                gtk_combo_box_text_append_text( GTK_COMBO_BOX_TEXT(widget), comboPtr);
                if ( (currentSetting != NULL) && (strcmp(comboPtr, currentSetting)==0) )
                {
                    gtk_combo_box_set_active( GTK_COMBO_BOX(widget), idx );
                }
                comboPtr = strtok(NULL, ",");
                idx++;
            }
            free(possibleValuesDup);
        }
        else
        {
            piboxLogger(LOG_ERROR, "Field %s is missing set values, defaulting to text entry field.\n", entry->name);
            widget = gtk_entry_new();
            if ( currentSetting != NULL )
                gtk_entry_set_text( GTK_ENTRY(widget), currentSetting);
        }
    }
    else if ( strcmp(entry->type, "range") == 0 )
    {
        // Spinner
        if ( possibleValues != NULL )
        {
            possibleValuesDup = strdup(possibleValues);
            lowStr = strtok(possibleValuesDup, "-");
            hiStr  = strtok(NULL, "-");
            if ( (lowStr != NULL) && ( hiStr != NULL) )
            {
                widget = gtk_spin_button_new_with_range((gdouble)atof(lowStr), (gdouble)atof(hiStr), 1);
                if ( currentSetting != NULL )
                    gtk_spin_button_set_value( GTK_SPIN_BUTTON(widget), (gdouble)atof(currentSetting) );
            }
            else
            {
                piboxLogger(LOG_ERROR, "Field %s is missing range low or high, defaulting to text entry field.\n", entry->name);
                widget = gtk_entry_new();
                if ( currentSetting != NULL )
                    gtk_entry_set_text( GTK_ENTRY(widget), currentSetting);
            }
            free(possibleValuesDup);
        }
        else
        {
            piboxLogger(LOG_ERROR, "Field %s is missing range, defaulting to text entry field.\n", entry->name);
            widget = gtk_entry_new();
            if ( currentSetting != NULL )
                gtk_entry_set_text( GTK_ENTRY(widget), currentSetting);
        }
    }
    else 
    {
        // Text Entry field
        widget = gtk_entry_new();
        if ( currentSetting != NULL )
            gtk_entry_set_text( GTK_ENTRY(widget), currentSetting);
    }
    gtk_widget_show(widget);

    // Add the widgets to the feature table.
    gtk_table_attach (GTK_TABLE (featureTable), label, 0, 1, currentRow, currentRow+1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
    gtk_table_attach (GTK_TABLE (featureTable), widget, 1, 2, currentRow, currentRow+1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
    currentRow++;
}

/*
 *========================================================================
 * Name:   chooseDevice
 * Prototype:  void chooseDevice( GtkTreeSelection *, gpointer )
 *
 * Description:
 * Use the selected device to build a configuration page and display it.
 *========================================================================
 */
#if 0
gboolean
chooseDevice (GtkTreeSelection *selection, 
                GtkTreeModel *model,
                GtkTreePath *path,
                gboolean is_selected,
                gpointer data)
#endif
static void
chooseDevice (GtkTreeSelection *selection, gpointer data)
{
    GtkTreeModel    *model;
    GtkTreeIter     iter;
    gchar           *device_ip;
    JSON_Value      *device_value;
    JSON_Object     *device_object;
    JSON_Value      *features_value;
    JSON_Object     *features_object;
    GSList          *featureList;
    char            *ptr;

    piboxLogger(LOG_INFO, "Entered.\n");
    if ( firstTime )
    {
        piboxLogger(LOG_INFO, "First time.\n");
        firstTime = 0;
        gtk_tree_selection_unselect_all (selection);
        return;
    }
    piboxLogger(LOG_INFO, "Not first time.\n");

    // if (gtk_tree_model_get_iter(model, &iter, path))
    if (gtk_tree_selection_get_selected (selection, &model, &iter))
    {
        // Find the matching entry in the video list
        piboxLogger(LOG_INFO, "Looking for device_ip.\n");
        gtk_tree_model_get (model, &iter, 0, &device_ip, -1);
        piboxLogger(LOG_INFO, "Found device_ip: %s\n", device_ip);

        // Retrieve the object for that device. 
        device_value = getDevice( device_ip );
        device_object = json_value_get_object(device_value);
        ptr = json_serialize_to_string(device_value);
        piboxLogger(LOG_INFO, "Device value: %s\n", ptr);
        free(ptr);

        // Get list of features and their types
        features_value = json_object_get_value( device_object, "features" );
        features_object = json_value_get_object(features_value);
        featureList = getFeatureList(features_object);

        // Remove the current device page, if any
        if ( devicePage != NULL )
            gtk_notebook_remove_page (GTK_NOTEBOOK(notebook), -1);

        // Add a new page with a table.
        featureTable = gtk_table_new (g_slist_length(featureList), 2, TRUE);
        gtk_widget_show (featureTable);
        gtk_notebook_append_page (GTK_NOTEBOOK (notebook), featureTable, NULL);

        // Create device fields in device page based on device configuration
        currentRow = 0;
        g_slist_foreach(featureList, genFields, features_object);

        // Switch tabs to the device page
        gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), 1);
        
        // Clean up the list
        g_slist_free_full( featureList, (GDestroyNotify)freeFeature);

        // Add Back and Accept buttons here!
        // TBD
    }
    
    gtk_tree_selection_unselect_all (selection);
    piboxLogger(LOG_INFO, "Exited.\n");
    return;
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a list of videos on the left and currently
 * selected video poster on the right.  The poster area is sensitive to 
 * clicks that will start playing the video.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *vbox1;
    GtkWidget           *sw;
    GtkListStore        *deviceStore;
    GtkCellRenderer     *renderer;
    GtkTreeSelection    *select;
    GtkTreeViewColumn   *col;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    // Add a notebook with two pages:
    // Page 1 is the list of devices and their icons (like videofe)
    // Page 2 is a device specific configuration page
    // Implement this as notebook pages

    notebook = gtk_notebook_new ();
    gtk_widget_show (notebook);
    gtk_container_add (GTK_CONTAINER (window), notebook);
    gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook), FALSE);

    vbox1 = gtk_vbox_new (FALSE, 20);
    gtk_widget_set_name (vbox1, "vbox1");
    gtk_widget_show (vbox1);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), vbox1, NULL);

    // List of devices will go here.
    deviceStore = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
    populateList(deviceStore);

    /*
     * Create a scrolled window for the tree view
     */
    sw = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start (GTK_BOX (vbox1), sw, TRUE, TRUE, 0);

    /*
     * Create the view of the video list
     */
    view = gtk_tree_view_new();
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(view), FALSE);
    renderer = gtk_cell_renderer_text_new();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
                                                 -1,      
                                                 NULL,  
                                                 renderer,
                                                 "text", 0,
                                                 NULL);
    renderer = gtk_cell_renderer_text_new();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (view),
                                                 -1,      
                                                 NULL,  
                                                 renderer,
                                                 "text", 0,
                                                 NULL);
    col = gtk_tree_view_get_column(GTK_TREE_VIEW(view), 1);
    gtk_tree_view_column_set_visible(col, FALSE);

    gtk_tree_view_set_model( GTK_TREE_VIEW(view), GTK_TREE_MODEL(deviceStore) );
    g_object_unref(G_OBJECT(deviceStore));
    gtk_container_add (GTK_CONTAINER (sw), view);

    /*
     * Setup for selecting from the view.
     */
    select = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
    gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
    g_signal_connect (G_OBJECT (select), "changed", G_CALLBACK (chooseDevice), NULL);

    // Make the main window die when destroy is issued.
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    // Now position the window and set its title
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 1024, 800);
    gtk_window_set_title(GTK_WINDOW(window), "iotapp");

    return window;
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int 
main(int argc, char *argv[])
{
    GtkWidget *window;
    struct stat stat_buf;
    char path[MAXBUF];

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    
    // Get keyboard behaviours for environment.
    loadKeysyms();

    piboxLogger(LOG_INFO, "Running from: %s\n", getcwd(path, MAXBUF));

    // Get list of devices
    getDevices();

    gtk_init(&argc, &argv);

    if ( isCLIFlagSet( CLI_TEST) && (stat("data/gtkrc", &stat_buf) == 0) )
        gtk_rc_parse("data/gtkrc");

    window = createWindow();
    gtk_widget_show_all(window);
    gtk_main();
    piboxLoggerShutdown();

    return 0;
}
