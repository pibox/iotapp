/*******************************************************************************
 * IoT App
 *
 * cli.c:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CLI_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sched.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <glib.h>
#include <pibox/log.h>
#include <pibox/utils.h>

#include "iotapp.h"

#define F_CFG   "/etc/iotapp.cfg"

static pthread_mutex_t cliOptionsMutex = PTHREAD_MUTEX_INITIALIZER;

/*========================================================================
 * Name:   parseArgs
 * Prototype:  void parseArgs( int, char ** )
 *
 * Description:
 * Parse the command line
 *
 * Input Arguments:
 * int argc         Number of command line arguments
 * char **argv      Command line arguments to parse
 *========================================================================*/
void
parseArgs(int argc, char **argv)
{
    int opt;

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, CLIARGS)) != -1 )
    {
        switch (opt)
        {
            /* -T: Use test files. */
            case 'T':
                cliOptions.flags |= CLI_TEST;
                break;

            /* Enable logging to local file. */
            case 'l':
                cliOptions.flags |= CLI_LOGTOFILE;
                cliOptions.logFile = optarg;
                break;

            /* -v: Verbose output (verbose is a library variable). */
            case 'v':
                cliOptions.verbose = atoi(optarg);
                break;

            default:
                printf("%s\nVersion: %s - %s\n", PROG, VERSTR, VERDATE);
                printf(USAGE);
                exit(0);
                break;
        }
    }
}

/*========================================================================
 * Name:   initConfig
 * Prototype:  void initConfig( void )
 *
 * Description:
 * Initialize run time configuration.  These are the default
 * settings.
 *========================================================================*/
void
initConfig( void )
{
    struct stat stat_buf;
    FILE *fd;
    char buf[MAXBUF];
    char *name;
    char *value;

    memset(&cliOptions, 0, sizeof(CLI_T));

    /* Are we running as root? */
    if ( getuid() == 0 )
        cliOptions.flags |= CLI_ROOT;

    // Check that the configuration file exists.
    if ( stat(F_CFG, &stat_buf) == 0 )
    {
        // Open the config file.
        fd = fopen(F_CFG, "r");
        if ( fd == NULL )
        {
            fprintf(stderr, "Failed to open %s: %s\n", F_CFG, strerror(errno));
            return;
        }

        // Each line is name:value pair
        while ( fgets(buf, MAXBUF, fd) != NULL )
        {
            piboxStripNewline(buf);
            name=strtok(buf, ":");
            value=strtok(NULL, ":");
            if ( (name == NULL) || (value==NULL) )
                continue;

            // Set verbosity level
            if ( strcmp(name, "verbose") == 0 )
                cliOptions.verbose = atoi(value);
        
            // Set debug file
            else if ( strcmp(name, "debugfile") == 0 )
            {
                cliOptions.logFile = g_strdup(value);
                cliOptions.flags |= CLI_LOGTOFILE;
            }
        }

        piboxLogger(LOG_INFO, "Log file: %s\n", buf);
    
        fclose(fd);
    }
}

/*========================================================================
 * Name:   validateConfig
 * Prototype:  void validateConfig( void )
 *
 * Description:
 * Validate configuration file used as input
 *========================================================================*/
void
validateConfig( void )
{
}

/*========================================================================
 * Name:   isCLIFlagSet
 * Prototype:  void isCLIFlagSet( int )
 *
 * Description:
 * Checks to see if an option is set in cliOptions.flags using a thread lock.
 * 
 * Returns;
 * 0 if requested flag is not set.
 * 1 if requested flag is set.
 *========================================================================*/
int
isCLIFlagSet( int bits )
{
    int status = 0;

    if ( cliOptions.flags & bits )
        status = 1;

    return status;
}

/*========================================================================
 * Name:   setCLIFlag
 * Prototype:  void setCLIFlag( int )
 *
 * Description:
 * Set options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
setCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags |= bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*========================================================================
 * Name:   unsetCLIFlag
 * Prototype:  void unsetCLIFlag( int )
 *
 * Description:
 * Unset options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
unsetCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags &= ~bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

