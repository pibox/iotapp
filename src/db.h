/*******************************************************************************
 * IoT App
 *
 * db.h:  Functions for dealing with devices
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef DB_H
#define DB_H

#include <pibox/parson.h>

// Use system wide keysym mappings
#define KEYSYMS_F     "/etc/pibox-keysyms"
#define KEYSYMS_FD    "data/pibox-keysyms"

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * PROTOTYPES
 *=======================================================================*/

#ifndef DB_C
extern void         getDevices (void );
extern JSON_Value   *getDevice ( char *device_ip );
extern void         populateList (GtkListStore *listStore);
#endif /* DB_C */

#endif /* !DB_H */

