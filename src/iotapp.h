/*******************************************************************************
 * IoT App
 *
 * iotapp.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef IOTAPP_H
#define IOTAPP_H

#include <gtk/gtk.h>

#define KEYSYMS_F     "/etc/pibox-keysyms"
#define KEYSYMS_FD    "data/pibox-keysyms"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef IOTAPP_C
int     daemonEnabled = 0;
char    errBuf[256];
gchar   *imagePath = NULL;
#else
extern int      daemonEnabled;
extern char     errBuf[];
extern gchar   *imagePath;
#endif /* IOTAPP_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "iotapp"
#define MAXBUF      4096

// Device field types define what kind of configuration the field supports
#define DEVICE_FIELD_MIN     0
#define DEVICE_FIELD_NUM     1
#define DEVICE_FIELD_TEXT    2
#define DEVICE_FIELD_RANGE   3
#define DEVICE_FIELD_SET     4
#define DEVICE_FIELD_MAX     DEVICE_TYPE_SET+1

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/
typedef struct _feature_t{
    char *name;
    char *type;
} FEATURE_T;

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include "cli.h"
#include "db.h"

#endif /* !IOTAPP_H */

